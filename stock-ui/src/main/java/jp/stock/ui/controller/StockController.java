package jp.stock.ui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.stock.ui.dao.StockDao;
import jp.stock.ui.domain.Stock;
import jp.stock.ui.domain.StockList;

@Controller
@RequestMapping("/stock")
public class StockController {

	@Autowired
	private StockDao stockDao;
	
	@GetMapping("/list")
	public String list(@RequestParam(required = false) String name, Model model) {
		// Employeeのfind APIをコールする
		StockList stockList = this.stockDao.find();

		// Thymeleafのテンプレートに渡すデータをemployeeListというキーで登録
		// 遷移先のhtmlにデータを渡す
		model.addAttribute("stockList", stockList);

		// list.htmlに遷移
		return "list";
	}
	
	@GetMapping(path = "/add")
	public String add(Model model) {
		model.addAttribute("addMode", true);
		return "edit";
	}
	@PostMapping(path = "/add")
	public String add(@ModelAttribute Stock stock, RedirectAttributes redirectAttributes) {
		// Employeeのadd APIをコールする
		this.stockDao.add(stock);

		// リダイレクト先に連携するメッセージを格納する
		redirectAttributes.addFlashAttribute("message", "在庫を追加しました。");

		// 従業員一覧にリダイレクトする
		return "redirect:/stock/list";
	}
	
	@GetMapping(path = "/edit")
	public String edit(@RequestParam int id, Model model) {
		model.addAttribute("editMode", true);
		// Stockのfind APIをコールする
		Stock stock = this.stockDao.get(id);

		// Thymeleafのテンプレートに渡すデータをstockListというキーで登録
		model.addAttribute("stock", stock);

		// edit.htmlに遷移
		return "edit";
	}
	
	@PostMapping(path = "/edit")
	public String edit(@ModelAttribute Stock stock, RedirectAttributes redirectAttributes) {
		// Stockのset APIをコールする
		this.stockDao.set(stock);
		
		// リダイレクト先に連携するメッセージを格納する
		redirectAttributes.addFlashAttribute("message", "在庫ID「" + stock.getId() + "」を更新しました。");

		// 従業員一覧にリダイレクトする
		return "redirect:/stock/list";
	}
	
	@PostMapping(path = "/delete")
	public String delete(@RequestParam int id, RedirectAttributes redirectAttributes) {
		// Employeeのremove APIをコールする
		this.stockDao.remove(id);

		// リダイレクト先に連携するメッセージを格納する
		redirectAttributes.addFlashAttribute("message", "在庫ID「" + id + "」を削除しました。");
				
		// 従業員一覧にリダイレクトする
		return "redirect:/stock/list";
	}

	
}
