package jp.stock.ui.dao;

import java.util.Collections;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;

import jp.stock.ui.configuration.ApiCallConfigurationProperties;
import jp.stock.ui.domain.Stock;
import jp.stock.ui.domain.StockList;

@Component
public class StockDaoImpl implements StockDao, InitializingBean {

	private static final Logger logger = LoggerFactory.getLogger(StockDaoImpl.class);
	
	@Autowired
	private ApiCallConfigurationProperties properties;

	@Autowired
	private RestOperations restOperations;

	private String stockApiUrlPrefix;

	@Override
	public StockList find() {
		// APIコールのURL
		String url = this.stockApiUrlPrefix;

		// APIコール
		return this.restOperations.getForObject(url, StockList.class);
	}
	
	@Override
	public Stock get(int id) {
		// APIコールのURL
		String getApiUrl = this.stockApiUrlPrefix + "/{id}";
		Map<String, Integer> params = Collections.singletonMap("id", id);

		// APIコール
		ResponseEntity<Stock> responseEntity = this.restOperations.getForEntity(getApiUrl, Stock.class, params);
		return responseEntity.getBody();
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.stockApiUrlPrefix = "http://" + this.properties.getHost() + ":" + this.properties.getPort()
				+ "/api/stocks";
	}
	
	@Override
	public void add(Stock stock) {
		// APIコールのURL
		String addApiUrl = this.stockApiUrlPrefix;

		// POSTリクエスト生成
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Stock> request = new HttpEntity<>(stock, headers);

		// APIコール
		this.restOperations.postForObject(addApiUrl, request, String.class);
	}
	
	@Override
	public void set(Stock stock) {
		// APIコールのURL
		String setApiUrl = this.stockApiUrlPrefix + "/{id}";
		Map<String, Integer> params = Collections.singletonMap("id", stock.getId());

		// POSTリクエスト生成
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Stock> request = new HttpEntity<>(stock, headers);

		// APIコール
		this.restOperations.patchForObject(setApiUrl, request, String.class, params);
	}
	
	@Override
	public void remove(int id) {
		// APIコールのURL
		String removeApiUrl = this.stockApiUrlPrefix + "/{id}";
		Map<String, Integer> params = Collections.singletonMap("id", id);
		// APIコール
		this.restOperations.delete(removeApiUrl, params);
	}
	

}
