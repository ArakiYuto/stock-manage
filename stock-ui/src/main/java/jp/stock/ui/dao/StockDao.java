package jp.stock.ui.dao;

import jp.stock.ui.domain.Stock;
import jp.stock.ui.domain.StockList;

public interface StockDao {
	//一覧表示
	StockList find();
	
	//一件参照
	Stock get(int id);
	
	//追加
	void add(Stock stock); 
	
	//更新
	void set(Stock stock);
	
	//削除
	void remove(int id);
}
