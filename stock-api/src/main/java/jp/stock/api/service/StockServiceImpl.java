package jp.stock.api.service;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import jp.stock.api.domain.Stock;
import jp.stock.api.domain.StockList;
import jp.stock.api.repository.StockRepository;

@Service
public class StockServiceImpl implements StockService {
	
	private final StockRepository repository;
	
	public StockServiceImpl(StockRepository repository) {
		this.repository = repository;
	}
	
	@Override
	public StockList find() {
		StockList stockList = new StockList();
		stockList.setStocks(this.repository.findList());
		return stockList;
	}
	
	@Override
	public Stock get(int id) {
		return this.repository.findOne(id);
	}

	
	@Override
	public void add(Stock stock) {
		this.repository.insert(stock);
	}
	
	@Override
	@Transactional(rollbackFor = Throwable.class)
	public void set(Stock stock) {
		this.repository.findOne(stock.getId());
		this.repository.update(stock);
	}
	
	@Override
	@Transactional(rollbackFor = Throwable.class)
	public void remove(int id) {
		Stock target = this.repository.findOne(id);
		this.repository.delete(target);
	}
	
	

}
