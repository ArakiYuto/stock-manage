package jp.stock.api.service;

import jp.stock.api.domain.Stock;
import jp.stock.api.domain.StockList;

public interface StockService {
	
	//在庫のデータリストを取得する
	StockList find();
	
	//一件参照
	Stock get(int id);
	
	//登録
	void add(Stock stock);
	
	//更新
	void set(Stock stock);
	
	//削除
	void remove(int id);
	
}
