package jp.stock.api.repository;

import java.util.List;


import jp.stock.api.domain.Stock;

public interface StockRepository {
	
	/**
	 * クエリパラメータによる複数件参照処理。
	 * @param name 名前
	 * @return 条件に合致した在庫データリスト
	 */
	List<Stock> findList();
	
	/**
	 * ID指定による１件参照処理。
	 * @param id リクエスト時のパスに含まれるID
	 * @return 指定されたIDのEmployeeデータ
	 */
	Stock findOne(int id);	
	
	//登録
	void insert(Stock stock);
	
	//更新
	void update(Stock stock);
	
	//削除
	void delete(Stock stock);
}
