package jp.stock.api.repository.mybatis;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.stock.api.domain.Stock;


public interface StockMapper {
	
	/**
	 * 全件検索
	 * @param name
	 * @return 検索結果
	 */
	List<Stock> find();
	
	/**
	 * 一件検索
	 * @param id
	 * @return 検索結果
	 */
	Stock get(@Param("id") int id);
	
	//登録
	int add(Stock stock);
	
	//更新
	int set(Stock stock);
	
	//削除
	int remove(Stock stock);

	
}
