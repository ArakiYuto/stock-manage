package jp.stock.api.repository;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.stock.api.domain.Stock;
import jp.stock.api.repository.mybatis.StockMapper;

@Repository
public class StockRepositoryImpl implements StockRepository {

	private static final Logger logger = LoggerFactory.getLogger(StockRepositoryImpl.class);

	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	@Override
	public List<Stock> findList() {
		return this.sqlSessionTemplate.getMapper(StockMapper.class).find();
	}

	@Override
	public Stock findOne(int id) {
		Stock stock = this.sqlSessionTemplate.getMapper(StockMapper.class).get(id);
		if (stock == null) {
			logger.info("Stock not found. id={}", id);
			// 自作の例外 ResourceNotFoundException
			throw new ResourceNotFoundException("Stock not found");
		}
		return stock;
	}

	// 登録
	@Override
	public void insert(Stock stock) {
		this.sqlSessionTemplate.getMapper(StockMapper.class).add(stock);
	}

	// 更新
	public void update(Stock stock) {
		int result = this.sqlSessionTemplate.getMapper(StockMapper.class).set(stock);
		if (result == 0) {
			logger.info("Stock not found. id={}", stock.getId());
			// 自作の例外 ResourceNotFoundException
			throw new ResourceNotFoundException("Stock not found");
		}
	}

	// 削除
	@Override
	public void delete(Stock stock) {
		int result = this.sqlSessionTemplate.getMapper(StockMapper.class).remove(stock);
		if (result == 0) {
			logger.info("Stock not found. id={}", stock.getId());
			// 自作の例外 ResourceNotFoundException
			throw new ResourceNotFoundException("Stock not found");
		}
	}

}
