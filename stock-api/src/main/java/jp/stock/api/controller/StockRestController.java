package jp.stock.api.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.stock.api.domain.Stock;
import jp.stock.api.domain.StockList;
import jp.stock.api.service.StockService;

@RestController
@RequestMapping("/api/stocks")
public class StockRestController {

	private final StockService service;

	public StockRestController(StockService service) {
		this.service = service;
	}

	@GetMapping(path = "", produces = "application/json")
	public StockList find() {
		return this.service.find();
	}
	
	/**
	 * ID指定による１件参照処理。
	 *
	 * @param id リクエスト時のパスに含まれるID
	 * @return 指定されたIDの在庫データ
	 */
	@GetMapping(path = "/{id}", produces = "application/json")
	//@PathVariable←URLの値を受け取る
	///api/employees/1だったら、取り出されるのは"1"
	public Stock get(@PathVariable int id) {
		return this.service.get(id);
	}

	@PostMapping(path = "", produces = "application/json")
	// Bodyに入っているstockの情報を受け取る
	public void add(@RequestBody Stock stock) {
		// addメソッドを呼ぶ
		this.service.add(stock);
	}
	
	/**
	 * ID指定による１件更新処理。
	 *
	 * @param id       リクエスト時のパスに含まれるID
	 * @param stock 更新内容
	 */
	@PatchMapping(path = "/{id}", produces = "application/json")
	public void set(@PathVariable int id, @RequestBody Stock stock) {
		stock.setId(id);
		this.service.set(stock);
	}
	
	/**
	 * ID指定による１件削除処理。
	 *
	 * @param id リクエスト時のパスに含まれるID
	 */
	@DeleteMapping(path = "/{id}", produces = "application/json")
	public void remove(@PathVariable int id) {
		this.service.remove(id);
	}


}
