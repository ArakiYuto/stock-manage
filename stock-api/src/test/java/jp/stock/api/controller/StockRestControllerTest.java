package jp.stock.api.controller;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import jp.stock.api.domain.Stock;
import jp.stock.api.domain.StockList;
import jp.stock.api.service.StockService;

public class StockRestControllerTest {

	@Mock
	private StockService stockService;

	@InjectMocks
	private StockRestController target;

	private MockMvc mockMvc;

	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(target).setMessageConverters().build();
	}

	@Test
	public void testFind() throws Exception {
		// setup: 事前準備
		StockList findResult = new StockList();

		// Mock呼び出し時の動作を定義
		Mockito.doReturn(findResult).when(stockService).find();

		// when: 対象のAPIリクエストを実行
		// performメソッド
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/stocks"))
				// 期待値の設定
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

		// then: テスト結果の検証。戻り値、Mockの呼び出し方法、回数など
		// UnitTestUtil←json形式で返ってきているかどうかの検証
		assertEquals(jp.stock.api.util.UnitTestUtil.entity2JsonText(findResult),
				result.getResponse().getContentAsString());
		Mockito.verify(stockService, Mockito.times(1)).find();

	}

	@Test
	public void testGet() throws Exception {
		// setup: 事前準備
		int id = 1;
		Stock getResult = new Stock();

		// Mock呼び出し時の動作を定義
		Mockito.doReturn(getResult).when(stockService).get(id);

		// when: 対象のAPIリクエストを実行
		// get("/api/employees/" + id.toString())←URLにパラメータを指定
		MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders.get("/api/stocks/" + id))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

		// then: テスト結果の検証。戻り値、Mockの呼び出し方法、回数など
		assertEquals(jp.stock.api.util.UnitTestUtil.entity2JsonText(getResult),
				result.getResponse().getContentAsString());
		Mockito.verify(stockService, Mockito.times(1)).get(id);

	}

	@Test
	public void testAdd() throws Exception {
		// setup
		// Mock
		Mockito.doNothing().when(stockService).add(ArgumentMatchers.any(Stock.class));

		// when
		// URLでメソッドが呼び出されているかの検証
		this.mockMvc.perform(MockMvcRequestBuilders.post("/api/stocks/").contentType(MediaType.APPLICATION_JSON)
				.content(jp.stock.api.util.UnitTestUtil.entity2JsonText(new Stock())));
		// then: テスト結果の検証。戻り値、Mockの呼び出し方法、回数など
		Mockito.verify(stockService, Mockito.times(1)).add(ArgumentMatchers.any(Stock.class));

	}

	@Test
	public void testSet() throws Exception {
		// setup
		int id = 1;
		// idの値が同じになっているかどうかの確認
		// {}内の結果が変数matcherに入る
		ArgumentMatcher<Stock> matcher = argument -> { // ラムダ式（無名関数）
			assertEquals(id, argument.getId());
			return true;
		};
		Mockito.doNothing().when(stockService).set(Mockito.argThat(matcher));

		// when
		mockMvc.perform(MockMvcRequestBuilders.patch("/api/stocks/" + id).contentType(MediaType.APPLICATION_JSON)
				.content(jp.stock.api.util.UnitTestUtil.entity2JsonText(new Stock())));

		// then
		Mockito.verify(stockService, Mockito.times(1)).set(Mockito.argThat(matcher));

	}

	@Test
	public void testRemove() throws Exception{
		// setup
		int id = 1;
		Mockito.doNothing().when(stockService).remove(id);

		// when
		mockMvc.perform(MockMvcRequestBuilders.delete("/api/stocks/" + id));

		// then
		Mockito.verify(stockService, Mockito.times(1)).remove(id);

	}

}
