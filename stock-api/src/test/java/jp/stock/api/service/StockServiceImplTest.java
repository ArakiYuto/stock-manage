package jp.stock.api.service;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import jp.stock.api.domain.Stock;
import jp.stock.api.domain.StockList;
import jp.stock.api.repository.StockRepository;

@RunWith(Enclosed.class)
public class StockServiceImplTest {

	public static class UseMockitoTest {
		// @Mockを付けてMock化する
		@Mock
		private StockRepository stockRepository;

		@Before
		public void before() throws Exception {
			MockitoAnnotations.initMocks(this);
		}

		@Test
		public void testFind() throws Exception {
			// setup
			// Mockが返す値を設定
			List<Stock> findResult = new ArrayList<>();
			StockServiceImpl target = new StockServiceImpl(this.stockRepository);

			// Mock呼び出し時の動作を定義
			Mockito.doReturn(findResult).when(stockRepository).findList();

			// when
			StockList stockList = target.find();

			// then
			assertEquals(findResult, stockList.getStocks());
			// findメソッドが一回呼ばれたかどうかの確認
			Mockito.verify(stockRepository, Mockito.times(1)).findList();
		}
		
		@Test
		public void testGet() throws Exception {
			// setup
			int input = 1;
			Stock findResult = new Stock();
			StockServiceImpl target = new StockServiceImpl(this.stockRepository);

			// Mock呼び出し時の動作を定義
			Mockito.doReturn(findResult).when(stockRepository).findOne(input);

			// when
			Stock result = target.get(input);

			// then
			assertEquals(findResult, result);
			Mockito.verify(stockRepository, Mockito.times(1)).findOne(input);
		}
		
		@Test
		public void testAdd() throws Exception {
			// setup
			Stock stock = new Stock();
			StockServiceImpl target = new StockServiceImpl(this.stockRepository);

			// 戻り値がないのでdoNothingを使う
			Mockito.doNothing().when(stockRepository).insert(stock);

			// when
			target.add(stock);

			// then
			Mockito.verify(stockRepository, Mockito.times(1)).insert(stock);
		}
		
		@Test
		public void testSet() throws Exception {
			// setup
			Stock stock = new Stock();
			stock.setId(1);
			StockServiceImpl target = new StockServiceImpl(this.stockRepository);

			// 戻り値がないのでdoNothingを使う
			Mockito.doReturn(stock).when(stockRepository).findOne(stock.getId());
			Mockito.doNothing().when(stockRepository).update(stock);

			// when
			target.set(stock);

			// then
			//　メソッドが一回ずつ呼ばれているかどうかの確認
			Mockito.verify(stockRepository, Mockito.times(1)).findOne(stock.getId());
			Mockito.verify(stockRepository, Mockito.times(1)).update(stock);
		}
		
		@Test
		public void testRemove() throws Exception {
			// setup
			int id = 1;
			Stock stock = new Stock();
			Mockito.doReturn(stock).when(stockRepository).findOne(id);
			Mockito.doNothing().when(stockRepository).delete(stock);
			StockServiceImpl target = new StockServiceImpl(stockRepository);
			
			// when
			target.remove(id);
			
			// then
			Mockito.verify(stockRepository, Mockito.times(1)).findOne(id);
			Mockito.verify(stockRepository, Mockito.times(1)).delete(stock);
		}
	}

}
