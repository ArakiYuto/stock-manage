package jp.stock.api.repository;

import static org.junit.Assert.*;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AbstractTestExecutionListener;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import jp.stock.api.domain.Stock;

@RunWith(Enclosed.class)
public class StockRepositoryImplTest {

	private static final String DATA_DIR = new File(
			System.getProperty("user.dir") + "\\src\\test\\resources\\repository") + File.separator;
	private static final File INIT_DATA = new File(DATA_DIR + "db_init.xml");

	/**
	 * SELECTを検証するテスト
	 */
	@RunWith(SpringRunner.class)
	@SpringBootTest(classes = jp.stock.api.ApiApplication.class)
	@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
			FindDbTest.FindTestExecutionListener.class })
	public static class FindDbTest extends AbstractTestExecutionListener {

		@Autowired
		private StockRepository target;

		// 全件検索をするためのメソッド
		@Test
		public void testFindAll() throws Exception {
			List<Stock> stocks = target.findList();

			// とってきた値が正しいかどうか検証する

			assertEquals(3, stocks.size());
			assertEquals(1, stocks.get(0).getId());
			assertEquals(2, stocks.get(1).getId());
			assertEquals(3, stocks.get(2).getId());
		}

		@Test
		public void testGet() throws Exception {
			Stock stock = target.findOne(1);
			assertNotNull("stock is null", stock);
			assertEquals(1, stock.getId());
			assertEquals("ヨコクノート", stock.getItem());

			jp.stock.api.util.UnitTestUtil.assertDateTimeUntilSeconds(stock.getCreatedOn(),
					LocalDateTime.parse("2020-07-01T10:00:00"));
			jp.stock.api.util.UnitTestUtil.assertDateTimeUntilSeconds(stock.getUpdatedOn(),
					LocalDateTime.parse("2020-07-02T13:00:00"));

		}

		@Test(expected = ResourceNotFoundException.class)
		public void testGetException() throws Exception {
			target.findOne(7);
		}

		/**
		 * テスト前処理、後処理を行なうクラス。 １．テストクラスがloadする前にDBをテスト用にセットアップする。
		 * ２．テストクラスの全ケース終了後にDBをテスト実行前の状態に戻す。
		 */
		static class FindTestExecutionListener extends AbstractTestExecutionListener {

			private static File backup;

			/**
			 * テストで利用するテーブルのバックアップを取得する。 テストで利用するデータをloadする。
			 */
			@Override
			public void beforeTestClass(TestContext testContext) throws Exception {
				DataSource source = getDataSource(testContext.getApplicationContext());
				// 今あるデータベースの値のバックアップを取る
				backup = new File(DATA_DIR + "STOCK_back.xml");
				jp.stock.api.util.DbUnitUtil.backup(source, backup, "STOCK");
				jp.stock.api.util.DbUnitUtil.loadData(source, INIT_DATA);
			}

			/**
			 * 取得したバックアップをリストアし、実行前の状態に復元する。
			 */
			@Override
			public void afterTestClass(TestContext testContext) throws Exception {
				// バックアップのデータを戻す
				jp.stock.api.util.DbUnitUtil.restoreBackup(getDataSource(testContext.getApplicationContext()), backup);
			}

			private DataSource getDataSource(ApplicationContext applicationContext) throws Exception {
				return applicationContext.getBean(jp.stock.api.configuration.DataSourceConfiguration.class)
						.dataSource();
			}
		}
	}

	/**
	 * INSERTのSQLを検証するテスト
	 */
	@RunWith(SpringRunner.class)
	@SpringBootTest(classes = jp.stock.api.ApiApplication.class)
	@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
			InsertDbTest.InsertTestExecutionListener.class })
	public static class InsertDbTest extends AbstractTestExecutionListener {

		private static final File EXPECTED_DATA = new File(DATA_DIR + "insert_db_expected.xml");

		@Autowired
		private StockRepository target;

		@Autowired
		private jp.stock.api.configuration.DataSourceConfiguration dataSourceConfiguration;

		@Test
		public void testInsert() throws Exception {
			Stock stock = new Stock();
			stock.setItem("ノート");
			stock.setPrice(100);
			stock.setQuantity(100);
			target.insert(stock);
			jp.stock.api.util.DbUnitUtil.assertMutateResult(dataSourceConfiguration.dataSource(), "STOCK",
					EXPECTED_DATA, Arrays.asList("ID", "CREATED_ON"));
		}

		/**
		 * テスト前処理、後処理を行なうクラス。 １．テストクラスがloadする前にDBをテスト用にセットアップする。
		 * ２．テストクラスの全ケース終了後にDBをテスト実行前の状態に戻す。
		 */
		static class InsertTestExecutionListener extends AbstractTestExecutionListener {

			private static File backup;

			/**
			 * テストで利用するテーブルのバックアップを取得する。 テストで利用するデータをloadする。
			 */
			@Override
			public void beforeTestClass(TestContext testContext) throws Exception {
				DataSource source = getDataSource(testContext.getApplicationContext());
				backup = new File(DATA_DIR + "STOCK_back.xml");
				jp.stock.api.util.DbUnitUtil.backup(source, backup, "STOCK");
				jp.stock.api.util.DbUnitUtil.loadData(source, INIT_DATA);
			}

			/**
			 * 取得したバックアップをリストアし、実行前の状態に復元する。
			 */
			@Override
			public void afterTestClass(TestContext testContext) throws Exception {
				jp.stock.api.util.DbUnitUtil.restoreBackup(getDataSource(testContext.getApplicationContext()), backup);
			}

			private DataSource getDataSource(ApplicationContext applicationContext) throws Exception {
				return applicationContext.getBean(jp.stock.api.configuration.DataSourceConfiguration.class)
						.dataSource();
			}
		}
	}

	/**
	 * UPDATE/REMOVE(論理削除)のSQLを検証するテスト
	 */
	@RunWith(SpringRunner.class)
	@SpringBootTest(classes = jp.stock.api.ApiApplication.class)
	@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
			ModifyDbTest.ModifyTestExecutionListener.class })
	public static class ModifyDbTest extends AbstractTestExecutionListener {

		private static final File UPDATE_EXPECTED_DATA = new File(DATA_DIR + "update_db_expected.xml");
		private static final File DELETED_EXPECTED_DATA = new File(DATA_DIR + "remove_db_expected.xml");

		@Autowired
		private StockRepository target;

		@Autowired
		private jp.stock.api.configuration.DataSourceConfiguration dataSourceConfiguration;

		@Test
		public void testUpdate() throws Exception {
			Stock stock = new Stock();
			stock.setId(1);
			stock.setItem("aaa");

			target.update(stock);
			jp.stock.api.util.DbUnitUtil.assertMutateResult(dataSourceConfiguration.dataSource(), "STOCK",
					UPDATE_EXPECTED_DATA,
					// 比較対象外
					Arrays.asList("UPDATED_ON"));
		}

		@Test(expected = ResourceNotFoundException.class)
		public void testUpdateException() throws Exception {
			Stock stock = new Stock();
			stock.setId(100);
			stock.setItem("Taro");

			target.update(stock);
			jp.stock.api.util.DbUnitUtil.assertMutateResult(dataSourceConfiguration.dataSource(), "STOCK",
					UPDATE_EXPECTED_DATA,
					// 比較対象外
					Arrays.asList("UPDATED_ON"));
		}

		@Test
		public void testDelete() throws Exception {
			Stock stock = new Stock();
			stock.setId(1);

			target.delete(stock);
			jp.stock.api.util.DbUnitUtil.assertMutateResult(dataSourceConfiguration.dataSource(), "STOCK",
					DELETED_EXPECTED_DATA,
					// 比較対象外
					Arrays.asList("DELETED_ON"));
		}

		@Test(expected = ResourceNotFoundException.class)
		public void testDeleteException() throws Exception {
			Stock stock = new Stock();
			stock.setId(100);

			target.delete(stock);
			jp.stock.api.util.DbUnitUtil.assertMutateResult(dataSourceConfiguration.dataSource(), "STOCK",
					DELETED_EXPECTED_DATA,
					// 比較対象外
					Arrays.asList("DELETED_ON"));
		}
	}

	/**
	 * テスト前処理、後処理を行なうクラス。 １．各テストメソッドの実行前にDBをテスト用にセットアップする。
	 * ２．各テストメソッドの実行後にDBをテスト実行前の状態に戻す。
	 *
	 * テーブルの更新、削除を複数のケースで検証するため、テストケース毎に上記の前処理、後処理を行なう。
	 */
	static class ModifyTestExecutionListener extends AbstractTestExecutionListener {

		private static File backup;

		/**
		 * テストで利用するテーブルのバックアップを取得する。 テストで利用するデータをloadする。
		 */
		@Override
		public void beforeTestMethod(TestContext testContext) throws Exception {
			DataSource source = getDataSource(testContext.getApplicationContext());
			backup = new File(DATA_DIR + "EMPLOYEE_back.xml");
			jp.stock.api.util.DbUnitUtil.backup(source, backup, "EMPLOYEE");
			jp.stock.api.util.DbUnitUtil.loadData(source, INIT_DATA);
		}

		/**
		 * 取得したバックアップをリストアし、実行前の状態に復元する。
		 */
		@Override
		public void afterTestMethod(TestContext testContext) throws Exception {
			jp.stock.api.util.DbUnitUtil.restoreBackup(getDataSource(testContext.getApplicationContext()), backup);
		}

		private DataSource getDataSource(ApplicationContext applicationContext) throws Exception {
			return applicationContext.getBean(jp.stock.api.configuration.DataSourceConfiguration.class).dataSource();
		}
	}

}
